﻿using System.Collections.Generic;

namespace SalesTax
{
    public class TaxRules
    {
        public static List<Tax> GetTaxesOfProduct(Product product)
        {
            var taxes = new List<Tax>();

            // VAT tax
            if (!(product.ProdType == ProductType.Book || 
                product.ProdType == ProductType.Food ||
                product.ProdType == ProductType.Medical))
            {
                taxes.Add(new Tax { Name = "VAT", Value = 0.10m });
            }

            // Imported tax
            if (product.ProdOrigin == ProductOrigin.Imported)
            {
                taxes.Add(new Tax { Name = "Imported", Value = 0.05m });
            }

            return taxes;
        }
    }
}
