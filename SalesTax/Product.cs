﻿namespace SalesTax
{
    public class Product
    {
        public string Name { get; set; }
        public decimal PriceBeforeTax { get; set; }
        public ProductType ProdType { get; set; }
        public ProductOrigin ProdOrigin { get; set; }

        public Product(string name, decimal price, ProductType prodType, ProductOrigin prodOrigin)
        {
            Name = name;
            PriceBeforeTax = price;
            ProdType = prodType;
            ProdOrigin = prodOrigin;
        }
    }
}
