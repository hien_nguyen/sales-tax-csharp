﻿namespace SalesTax
{
    public enum ProductOrigin
    {
        Imported,
        Local
    }
}
