﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace SalesTax
{
    class Program
    {
        static void Main(string[] args)
        {
            PurchaseOrder1();
        }

        private static void PurchaseOrder1()
        {
            // select order
            var order = new Order();
            var orderItem1 = new OrderItem(new Product("imported box of chocolates", 10.0m, ProductType.Food, ProductOrigin.Imported), 1);
            var orderItem2 = new OrderItem(new Product("imported bottle of perfume", 47.50m, ProductType.Other, ProductOrigin.Imported), 1);
            order.AddOrderItem(orderItem1);
            order.AddOrderItem(orderItem2);

            // purchase
            order.Checkout();

            // output
            // show order info
            PrintOrder(order);
        }

        private static void PrintOrder(Order order)
        {
            foreach (var orderItem in order.OrderItems)
            {
                Console.WriteLine("{0}, {1}, {2:F}", orderItem.OrderQuantity, orderItem.OrderProduct.Name,
                    orderItem.TotalPriceAfterTax);
            }

            Console.WriteLine("\nSales Taxes: {0:F}", order.SalesTaxes);
            Console.WriteLine("Total: {0:F}", order.TotalPriceAfterTax);
        }
    }
}
