﻿namespace SalesTax
{
    public enum ProductType
    {
        Other,
        Book,
        Food,
        Medical
    }
}
