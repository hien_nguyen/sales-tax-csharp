﻿using System.Collections.Generic;
using System.Linq;

namespace SalesTax
{
    public class Order
    {
        public List<OrderItem> OrderItems  { get; private set; }
        public decimal TotalPriceAfterTax { get; private set; }
        public decimal SalesTaxes { get; private set; }

        public void CalculateTotalPrice()
        {
            TotalPriceAfterTax = OrderItems.Sum(order => order.TotalPriceAfterTax);
        }

        public void CalculateSalesTaxes()
        {
            SalesTaxes = OrderItems.Sum(order => order.SalesTaxes);
        }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public void AddOrderItem(OrderItem orderItem)
        {
            OrderItems.Add(orderItem);
        }

        public void Checkout()
        {
            foreach (var orderItem in OrderItems)
            {
                orderItem.CalculatePrice();
            }

            TotalPriceAfterTax = OrderItems.Sum(order => order.TotalPriceAfterTax);

            SalesTaxes = OrderItems.Sum(order => order.SalesTaxes);
        }
    }
}
