﻿using System;
using System.Linq;

namespace SalesTax
{
    public class OrderItem
    {
        public Product OrderProduct { get; set; }
        public int OrderQuantity { get; set; }

        public decimal TotalPriceAfterTax { get; private set; }

        public decimal SalesTaxes { get; private set; }

        public OrderItem(Product product, int quantity)
        {
            OrderProduct = product;
            OrderQuantity = quantity;
        }

        public void CalculatePrice()
        {
            var taxes = TaxRules.GetTaxesOfProduct(OrderProduct);
            SalesTaxes = Math.Ceiling(OrderProduct.PriceBeforeTax * taxes.Sum(tax => tax.Value) * 20) / 20.0m * OrderQuantity;
            TotalPriceAfterTax = OrderProduct.PriceBeforeTax * OrderQuantity + SalesTaxes;
        }
    }
}
