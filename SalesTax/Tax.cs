﻿namespace SalesTax
{
    public class Tax
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
