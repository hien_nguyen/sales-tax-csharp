﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SalesTax.Tests
{
    [TestClass]
    public class OrderTests
    {
        [TestMethod]
        public void PurchaseOrder1()
        {
            /*
            ## Input 1
            Quantity, Product, Price
            1, book, 12.49
            1, music cd, 14.99
            1, chocolate bar, 0.85
             */
            var orderItem1 = new OrderItem(new Product("book", 12.49m, ProductType.Book, ProductOrigin.Local), 1);
            var orderItem2 = new OrderItem(new Product("music cd", 14.99m, ProductType.Other, ProductOrigin.Local), 1);
            var orderItem3 = new OrderItem(new Product("chocolate bar", 0.85m, ProductType.Food, ProductOrigin.Local), 1);

            var order = new Order();
            order.AddOrderItem(orderItem1);
            order.AddOrderItem(orderItem2);
            order.AddOrderItem(orderItem3);
            order.Checkout();

            Assert.AreEqual(12.49m, orderItem1.TotalPriceAfterTax);
            Assert.AreEqual(16.49m, orderItem2.TotalPriceAfterTax);
            Assert.AreEqual(0.85m, orderItem3.TotalPriceAfterTax);
            Assert.AreEqual(1.50m, order.SalesTaxes);
            Assert.AreEqual(29.83m, order.TotalPriceAfterTax);
        }

        [TestMethod]
        public void PurchaseOrder2()
        {
            /*
            ## Input 2
            Quantity, Product, Price
            1, imported box of chocolates, 10.00
            1, imported bottle of perfume, 47.50
             */

            var orderItem1 = new OrderItem(new Product("imported box of chocolates", 10.0m, ProductType.Food, ProductOrigin.Imported), 1);
            var orderItem2 = new OrderItem(new Product("imported bottle of perfume", 47.50m, ProductType.Other, ProductOrigin.Imported), 1);
            
            var order = new Order();
            order.AddOrderItem(orderItem1);
            order.AddOrderItem(orderItem2);
            order.Checkout();

            Assert.AreEqual(orderItem1.TotalPriceAfterTax, 10.50m);
            Assert.AreEqual(orderItem2.TotalPriceAfterTax, 54.65m);
            Assert.AreEqual(7.65m, order.SalesTaxes);
            Assert.AreEqual(65.15m, order.TotalPriceAfterTax);
        }

        [TestMethod]
        public void PurchaseOrder3()
        {
            /*
            ## Input 3
            Quantity, Product, Price
            1, imported bottle of perfume, 27.99
            1, bottle of perfume, 18.99
            1, packet of headache pills, 9.75
            1, box of imported chocolates, 11.25
             * */
            var orderItem1 = new OrderItem(new Product("imported bottle of perfume", 27.99m, ProductType.Other, ProductOrigin.Imported), 1);
            var orderItem2 = new OrderItem(new Product("bottle of perfume", 18.99m, ProductType.Other, ProductOrigin.Local), 1);
            var orderItem3 = new OrderItem(new Product("packet of headache pills", 9.75m, ProductType.Medical, ProductOrigin.Local), 1);
            var orderItem4 = new OrderItem(new Product("box of imported chocolates", 11.25m, ProductType.Food, ProductOrigin.Imported), 1);
            
            var order = new Order();
            order.AddOrderItem(orderItem1);
            order.AddOrderItem(orderItem2);
            order.AddOrderItem(orderItem3);
            order.AddOrderItem(orderItem4);
            order.Checkout();

            Assert.AreEqual(orderItem1.TotalPriceAfterTax, 32.19m);
            Assert.AreEqual(orderItem2.TotalPriceAfterTax, 20.89m);
            Assert.AreEqual(orderItem3.TotalPriceAfterTax, 9.75m);
            Assert.AreEqual(orderItem4.TotalPriceAfterTax, 11.85m);
            Assert.AreEqual(6.70m, order.SalesTaxes);
            Assert.AreEqual(74.68m, order.TotalPriceAfterTax);
        }
    }
}
